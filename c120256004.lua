local m=120256004
local list={120207008,120237008}
local cm=_G["c"..m]
cm.name="帝王龙"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Fusion Material
	RD.AddFusionProcedure(c,list[1],list[2])
end