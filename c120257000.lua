local m=120257000
local list={120145000,120125001}
local cm=_G["c"..m]
cm.name="恶魔黑龙"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Fusion Material
	RD.AddFusionProcedure(c,list[1],list[2])
end